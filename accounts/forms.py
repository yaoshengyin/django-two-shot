from django import forms
from django.contrib.auth.models import User


class LogInForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(max_length=150, widget=forms.PasswordInput)


class SignUpForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(widget=forms.PasswordInput(), max_length=150)
    password_confirmation = forms.CharField(
        widget=forms.PasswordInput(), max_length=150
    )

    class Meta:
        model = User
        fields = ["username", "password", "password_confirmation"]
