from django.shortcuts import render, redirect
from receipts.models import Account, Receipt, ExpenseCategory
from receipts.forms import AccountForm, CategoryForm, ReceiptForm
from django.contrib.auth.decorators import login_required 

# Create your views here.


@login_required
def receipt_list(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {'receipts': receipts}
    return render(request, "receipts/list.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = CategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect("category_list")
    else:
        form = CategoryForm()
    context = {
        "form": form,
    }
    return render(request, "categories/create.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = CategoryForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/create.html", context)


@login_required
def category_list(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    category_data = []
    for category in categories:
        receipt_count = Receipt.objects.filter(category=category).count()
        category_data.append(
            {"category": category, "receipt_count": receipt_count}
            )
    context = {"category_data": category_data}
    return render(request, "categories/list.html", context)


@login_required
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)
    account_data = []
    for account in accounts:
        receipt_count = Receipt.objects.filter(account=account).count()
        account_data.append(
            {"account": account, "receipt_count": receipt_count}
        )
    context = {"account_data": account_data}
    return render(request, "accounts/list.html", context)